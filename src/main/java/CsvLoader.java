import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.elasticsearch.spark.sql.api.java.JavaEsSparkSQL;

import java.util.*;

/**
 * Implements a Spark job that defines ElasticSearch DataSource (DS) from:
 * 1. Generated data, if the data source size is provided.
 * 2. Specified CSV file path.
 * Loads CSV from specified path into a collection of data points
 * Infers DS schema and creates ElasticSearch index
 * Persists loaded data to created ES index
 */
public class CsvLoader {

    private static final Logger logger = LogManager.getLogger(CsvLoader.class);

    public static void main(String args[]) {
        if (args.length < 3) {
            logger.error("Usage: " +
                                 "CsvLoader " +
                                 "<DS-name(string)> <CSV-file-path(string)> <has-header('true'/'false')> " +
                                 "<optional:size(int)>");
            System.exit(1);
        }
        String dsName = StringUtils.lowerCase(args[0]);
        String csvFilePath = args[1];
        String containsHeader = args[2];
        logger.info("Arguments: {}, {}, {}", dsName, csvFilePath, containsHeader);

        JavaSparkContext ctx = new JavaSparkContext();

        if (args.length == 4) {
            logger.info("Generating {} data points.", args[3]);
            JavaRDD<Map<String, String>> rdd = ctx.parallelize(generate(Integer.valueOf(args[3]), dsName));

            logger.info("Indexing generated source data.");
            JavaEsSpark.saveToEs(rdd, dsName + "/source");
        } else {
            logger.info("Parsing CSV file at location: {}", csvFilePath);
            SparkSession session = SparkSession.builder().getOrCreate();
            Dataset<Row> csv = session.read()
                                      .format("csv")
                                      .option("header", containsHeader)
                                      .load(csvFilePath);
            csv.printSchema();
            csv.show();

            logger.info("Indexing parsed CSV data to {}.", dsName);
            JavaEsSparkSQL.saveToEs(csv, String.format("%s/source", dsName));
        }

        ctx.close();
    }

    /**
     * Simple source data generator.
     * Returns a list of generated table rows, modeled as a map of key-value pairs (key = column name).
     * ds_name_col_1 key contains a list-wide unique value (ID).
     *
     * @param size - size of the list
     * @param prefix - data source name
     * @return list of generated table rows
     */
    private static List<Map<String, String>> generate(int size, String prefix) {
        List<Map<String, String>> points = new ArrayList<>();
        if (size <= 0)
            return points;
        Random rnd = new Random();
        int temp = 0;
        while (size != 0) {
            Map<String, String> point = new HashMap<>();
            int number = rnd.nextInt(3) + 1;
            point.put(prefix + "_col_1", String.valueOf(temp + number));
            point.put(prefix + "_col_2", RandomStringUtils.randomAlphabetic(5));
            point.put(prefix + "_col_3", RandomStringUtils.randomAlphabetic(5));
            points.add(point);
            temp += number;
            size--;
        }
        return points;
    }
}
