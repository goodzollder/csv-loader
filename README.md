# CSV-LOADER
Csv-loader spark job implementation.  
This project is a part of the data source joiner application bundle.  
See [DS-BUILDER](https://bitbucket.org/goodzollder/ds-builder/src/master/) project for details.  

### Description

CSV-LOADER is a simple Java application that runs on a target Spark cluster (worker).  
Based on the list of arguments received from the driver applications, it performs the following:  

* If _generateDs_ is _true_
    * generates column names as <ds-name> + "_col_1"/"_col_2"/"_col_3"
    * populates the 1st column with unique numeric values (as strings)
    * populates other columns with random 5-char long alphabetic values
    * the number of generated rows corresponds to specified data source _size_
    * loads generated data into a JavaRDD dataset

* If _generateDs_ is _false_
    * Creates a dataset from the CSV at specified path

* Saves resulting dataset into elasticsearch as a new data source (index).  

### Installation
The simplest way to start using the application is to run it in a bundle.  
See [DS-BUILDER](https://bitbucket.org/goodzollder/ds-builder/src/master/) project for details.    
Csv-loader jar can also be manually generated and copied into the shared volume for further consumption by a Spark worker.

##### Build manually

From the project's root, generate csv-loader jar and copy it into ```ds-builder/spark-docker/volumes/jars``` shared volume.  
```bash
    ./gradlew clean build
    cp build/libs/java -jar build/libs/<jar-name> ../spark-docker/volumes/jars/
```
